#!/usr/bin/env python3

import os
import subprocess
import signal
import time

LOGGER_PATH = "./logger"
LOG_PREFIX = "testlogs"

class Logger:
    def __init__(self, path, prefix):
        self._prefix = prefix
        self._logger = subprocess.Popen([path, prefix], stdin=subprocess.PIPE,
                                        #stdout=subprocess.PIPE,
                                        #stderr=subprocess.DEVNULL,
                                        encoding="utf8")
        self._track = [""]
        time.sleep(1)

    def feed(self, string):
        self._logger.stdin.write(string)
        self._track[-1] += string

    def rotate(self):
        self._logger.stdin.flush()
        time.sleep(1)
        self._logger.send_signal(signal.SIGHUP)
        time.sleep(1)
        self._track.append("")

    def close(self):
        self._logger.stdin.close()
        self._logger.wait()
        assert self._logger.returncode == 0

    def verify(self, check_last=True):
        if not check_last:
            self._logger.stdin.flush()
        time.sleep(1)
        def content_differ(name, content):
            with open(name, "r") as f:
                s = f.read()
                return s != content
                
        if check_last:
            filename = self._prefix
            content = self._track[-1]
            if content_differ(filename, content):
                self.close()
                raise AssertionError("Content in " + filename + " != " + content)

        for i in range(len(self._track) - 1):
            need_to_raise = False
            filename = "{}.{}".format(self._prefix, i + 1)
            content = self._track[-2 - i]
            if content_differ(filename, content):
                self.close()
                raise AssertionError("Content in " + filename + " != " + content)

def test_logger_basics():
    logger = Logger(LOGGER_PATH, LOG_PREFIX)
    logger.feed("Holy cow, Rick! I didn't know hanging out with you was making me smarter!\n")
    logger.feed("Full disclosure, Morty - it's not. Temporary superintelligence is just a side effect of the Megaseeds dissolving in your rectal cavity.\n")
    logger.feed("Aw, man!\n")
    logger.rotate()
    logger.feed("Yeah. And once those seeds weahh-wear off, you're gonna lose most of your motor skills and... you're also gonna lose a significant amount of brain functionality for 72 hours, Morty.\n")
    logger.verify(False)
    logger.rotate()
    logger.feed("Starting ruh-ight about now.\n")
    logger.verify(False)
    logger.feed("Ohh, man! ")
    logger.feed("Ohh, ohhh geez!! ")
    logger.feed("Ohh...\n")
    logger.close()
    logger.verify()

if __name__ == "__main__":
    test_logger_basics()
