#pragma once

#include "stddef.h"

void encrypt(char *data, size_t len);
void decrypt(char *data, size_t len);
