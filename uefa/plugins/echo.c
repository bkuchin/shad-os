#include "plugin.h"

void echo(struct stream *stream)
{
    char buffer[1024];
    size_t size;
    while ((size = stream_read(stream, buffer, 1024)) > 0) {
        stream_write(stream, buffer, size);
    }
}

void uefa_plugin_encode(struct stream *stream)
{
    echo(stream);
}

int uefa_plugin_decode(struct stream *stream)
{
    echo(stream);
    return 0;
}
