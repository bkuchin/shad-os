#!/usr/bin/env python3

import sys
import subprocess

BINARY_PATH = "./build/uefa"

class App:
    def __init__(self, codec):
        self._codec = codec

    def _run(self, args, stdin):
        process = subprocess.Popen([BINARY_PATH, self._codec] + args, stdin=subprocess.PIPE,
                                   stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = process.communicate(stdin, timeout=3)
        assert not stderr, f"Stderr is not empty:\n{stderr}"
        assert process.returncode == 0
        return stdout

    def encrypt(self, data):
        return self._run([], data)

    def decrypt(self, data):
        return self._run(["-d"], data)

    def get_error(self):
        process = subprocess.Popen([BINARY_PATH, self._codec], stdin=subprocess.PIPE,
                                   stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = process.communicate(b"abcdef", timeout=3)
        assert not stdout, f"Stdout is not empty:\n{stdout}"
        assert process.returncode != 0
        return stderr

def test_null():
    app = App("null")
    assert app.encrypt(b"hello, world!") == b""
    assert app.decrypt(b"foobar") == b""

def test_hex():
    app = App("hex")
    assert app.encrypt(b"hello, world\n") == b"261000e100b502c2f6c6c65686"
    assert app.decrypt(b"b6f1c1612445d086470237f6b616") == b"akos the best\n"
    assert app.decrypt(app.encrypt(b"12345" * 500)) == b"12345" * 500

def test_echo():
    app = App("echo")
    assert app.encrypt(b"never let me go") == b"\x01\x02V\x00\x1f\x00\x18el reven"
    assert app.decrypt(b"'\x11\x15FT\x1c\x16NE\x18\t\x13\x06\x12LR\x16\x18mus lanrete yht tuB") \
            == b"But thy eternal summer shall not fade"
    assert app.decrypt(app.encrypt(b"abcdef" * 1000)) == b"abcdef" * 1000

def test_poem():
    app = App("poem")
    assert app.encrypt(b"abc\n") == \
            b"Tossing their heads in sprightly dance.\n" \
            b"Continuous as the stars that shine\n" \
            b"That floats on high o'er vales and hills,\n" \
            b"I wandered lonely as a cloud\n" \
            b"When all at once I saw a crowd,\n" \
            b"Continuous as the stars that shine\n" \
            b"That floats on high o'er vales and hills,\n" \
            b"Continuous as the stars that shine\n"
    data = b"A poet could not but be gay,\n" \
           b"Continuous as the stars that shine\n" \
           b"A host, of golden daffodils;\n" \
           b"I wandered lonely as a cloud\n" \
           b"Fluttering and dancing in the breeze.\n" \
           b"Continuous as the stars that shine\n" \
           b"Beside the lake, beneath the trees,\n" \
           b"Continuous as the stars that shine\n"
    assert app.decrypt(data) == b"def\n"
    text = b"Surely You're Joking, Mr. Feynman!"
    assert app.decrypt(app.encrypt(text * 50)) == text * 50

def test_errors():
    assert b"Plugin error: not an ELF" in App("not_elf").get_error()
    assert b"Plugin error: missing 'uefa_plugin_encode' or 'uefa_plugin_decode'" \
            in App("broken").get_error()
    assert b"Plugin error: undefined symbol" in App("reverse").get_error()

if __name__ == "__main__":
    globals()["test_" + sys.argv[1]]()
