#include "syscall.h"

int main()
{
    int pipefd[2];
    char buf[1000];
    pipe(pipefd);
    write(pipefd[1], "test", 4);
    ssize_t r = read(pipefd[0], buf, 4);
    if (r != 4 || buf[0] != 't' || buf[1] != 'e' || buf[2] != 's' || buf[3] != 't')
        return 1;
    return 0;
}
